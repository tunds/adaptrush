package com.example.tundeadegoroye.adaptrush.Models;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.example.tundeadegoroye.adaptrush.R;

import java.util.LinkedHashMap;
import java.util.Random;

/**
 * Created by tundeadegoroye on 18/11/2016.
 */


public class GameObject extends AppCompatActivity {

    // Hashable object to hold the correct environment and animal
    private LinkedHashMap<Environment, Animal> gameObjectMap = new LinkedHashMap<>();
    // The generated number for the speed of the object
    private int genNum, gameSpeed;
    // The database variable
    private DatabaseManager db;


    public GameObject(Context context) {

        db = new DatabaseManager(context);

        // Configure difficulty
        switch (db.getDifficulty()){
            case "Easy":
                gameSpeed = 2;
                break;
            case "Medium":
                gameSpeed = 3;
                break;
            case "Hard":
                gameSpeed = 5;
                break;
        }

        // Cold animals
        gameObjectMap.put(
                new Environment(ContextCompat.getDrawable(context, R.drawable.cold), new String[] {"penguin"}),
                new Animal(gameSpeed,gameSpeed,0,0,ContextCompat.getDrawable(context, R.drawable.penguin),"penguin", "Penguins are a group of aquatic, flightless birds living almost exclusively in the Southern Hemisphere, especially in Antarctica.")
        );

        // Sunny animals
        gameObjectMap.put(
                new Environment(ContextCompat.getDrawable(context, R.drawable.sunny), new String[] {"rabbit"}),
                new Animal(gameSpeed,gameSpeed,0,0,ContextCompat.getDrawable(context, R.drawable.rabbit),"rabbit", "Rabbits are small mammals in the family Leporidae of the order Lagomorpha, found in several parts of the world.")
        );

        // Desert animals
        gameObjectMap.put(
                new Environment(ContextCompat.getDrawable(context, R.drawable.dessert), new String[] {"panther", "rhino", "camel"}),
                new Animal(gameSpeed,gameSpeed,0,0,ContextCompat.getDrawable(context, R.drawable.panther),"panther", "A black panther is the melanistic color variant of any Panthera species. Black panthers in Asia and Africa are leopards (Panthera pardus) and black panthers in the Americas are black jaguars (Panthera onca).")
        );

        gameObjectMap.put(
                new Environment(ContextCompat.getDrawable(context, R.drawable.dessert), new String[] {"panther", "rhino", "camel"}),
                new Animal(gameSpeed,gameSpeed,0,0,ContextCompat.getDrawable(context, R.drawable.rhino),"rhino","A rhinoceros, often abbreviated to rhino, is one of any five extant species of odd-toed ungulates in the family Rhinocerotidae, as well as any of the numerous extinct species.")
        );

        gameObjectMap.put(
                new Environment(ContextCompat.getDrawable(context, R.drawable.dessert),new String[] {"panther", "rhino", "camel"}),
                new Animal(gameSpeed,gameSpeed,0,0,ContextCompat.getDrawable(context, R.drawable.camel),"camel","A camel is an even-toed ungulate within the genus Camelus, bearing distinctive fatty deposits known as 'humps' on its back.")
        );

    }

    /**
     * This method is used to get all the game objects filled with their enviroment and animals
     * @return LinkedHashMap<Environment, Animal>
     */
    public LinkedHashMap<Environment, Animal> getGameObjectMap() {
        return gameObjectMap;
    }
}
