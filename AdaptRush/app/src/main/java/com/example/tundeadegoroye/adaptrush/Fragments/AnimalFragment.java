package com.example.tundeadegoroye.adaptrush.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.tundeadegoroye.adaptrush.Models.Animal;
import com.example.tundeadegoroye.adaptrush.R;

import java.util.ArrayList;

public class AnimalFragment extends Fragment {

    // ArrayList to hold the animals for recycle view
    private ArrayList<Animal> animals;

    /**
     * This method is used to assign the local animals variable
     * @param animals This is the first paramter to assign the animals
     * @return null
     */
    public void setAnimals(ArrayList<Animal> animals)
    {
        this.animals=animals;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_animal_list, container, false);

        // Set the adapter using the animal adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new AnimalAdapter(animals));

        }
        return view;
    }
}


class AnimalAdapter extends RecyclerView.Adapter {

    // Animals ArrayList
    private ArrayList<Animal> animals;

    // Contstruct to assign the animal variable
    public AnimalAdapter(ArrayList<Animal> animals) {
        this.animals = animals;
    }

    // Get the relative layout from the layout file animals_info
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.animals_info, parent, false);
        MyViewHolder vh = new MyViewHolder( (RelativeLayout) v);
        return vh;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // The UI components
        MyViewHolder vh = (MyViewHolder) holder;
        TextView title = (TextView) vh.itemView.findViewById(R.id.animalTitleTxtVw);
        ImageView img = (ImageView) vh.itemView.findViewById(R.id.animalImgVw);
        TextView info = (TextView) vh.itemView.findViewById(R.id.animalInfoTxtVw);

        // Assign some data to the UI components
        title.setText(animals.get(position).getAnimalType());
        info.setText(animals.get(position).getInfo());
        img.setImageDrawable(animals.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return animals.size();
    }

    // Inner class to get the layout passed through constructor
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout layout;
        public MyViewHolder(RelativeLayout layout) {
            super(layout);
            this.layout = layout;
        }
    }

}

