package com.example.tundeadegoroye.adaptrush.Models;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.example.tundeadegoroye.adaptrush.R;

/**
 * Created by tundeadegoroye on 20/11/2016.
 */

public class Health {

    // Variable to hold the max health
    private int health = 30;

    // Images for the health
    private ImageView[] healthImgs;

    // Activity passed into this class
    private Context context;

    public Health(ImageView[] healthImgs, Context context) {
        this.healthImgs = healthImgs;
        this.context = context;
    }

    /**
     * This method is used to decrease the health
     * @param health The amount to take of the health
     * @return null
     */
    public void decreaseHealth(int health) {
        this.health -= health;
    }

    /**
     * This method is used to change the health images
     * @param health The amount to take health the user has
     * @return null
     */
    public void decreaseHealthImgs(int health) {

        // Depending on the health change the image
        if (health == 20){
            healthImgs[0].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.heartoutline));
        } else if(health == 10){
            healthImgs[1].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.heartoutline));
        } else {
            healthImgs[2].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.heartoutline));
        }

    }

    /**
     * This method is used to get the current health
     * @return int
     */
    public int getHealth() {
        return health;
    }
}
