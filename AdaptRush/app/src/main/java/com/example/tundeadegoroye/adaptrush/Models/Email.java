package com.example.tundeadegoroye.adaptrush.Models;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

/**
 * Created by tundeadegoroye on 22/11/2016.
 */

public class Email {

    // Strings for holding information about the email
    private String email, subject;

    // The activity
    private Context context;

    public Email(Context context, String email, String subject) {
        this.context = context;
        this.email = email;
        this.subject = subject;
    }

    /**
     * This method is used to create the email
     * @return null
     */
    public void createEmail(){

        // Help from http://stackoverflow.com/questions/14331953/no-activity-found-to-handle-intent-while-sending-email

        // Create email intent and assign data
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.setData(Uri.parse("mailto:"+email));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);

        // Start the intent or log an error
        try {
            context.startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
            Log.v("Error: ", e.toString());
        }
    }
}
