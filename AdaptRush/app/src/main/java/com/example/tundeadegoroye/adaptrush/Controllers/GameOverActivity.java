package com.example.tundeadegoroye.adaptrush.Controllers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tundeadegoroye.adaptrush.Models.DatabaseManager;
import com.example.tundeadegoroye.adaptrush.R;
import com.example.tundeadegoroye.adaptrush.Models.User;

public class GameOverActivity extends Misc {

   // Text fields for score and name text field
   protected TextView scoreTxtView;
   protected EditText nameTxtField;

   // Variable to access the database
   protected DatabaseManager db;

   // Button to save score
   protected Button saveBtn;

   // Anon class listener for save button
   protected View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // Check to make sure the text field isn't empty
            if(!nameTxtField.getText().toString().isEmpty()){

                // Insert new data into the database
                db.addScore(new User(nameTxtField.getText().toString(), getIntent().getIntExtra("Score",0)));

                // Disable the button and display a message to the user
                saveBtn.setEnabled(false);
                Toast.makeText(getApplicationContext(), "Saved score", Toast.LENGTH_SHORT).show();
                saveBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorDarkAccent));

            } else {

                // Display a message to the user
                Toast.makeText(getApplicationContext(), "Name empty", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.end_navigation_draw);

        // Get the UI components
        nameTxtField = (EditText) findViewById(R.id.nameTxtEdit);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        scoreTxtView = (TextView) findViewById(R.id.scoreTxtVw);

        // Instantiate the databases
        db = new DatabaseManager(this);

        // Set the score from data passed from previous activity
        scoreTxtView.setText("Score: " + getIntent().getIntExtra("Score",0));

        // Assign the listener to the button
        saveBtn.setOnClickListener(listener);

        // Gen code for the navigation draw
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

}
