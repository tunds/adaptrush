package com.example.tundeadegoroye.adaptrush.Controllers;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.tundeadegoroye.adaptrush.GameSurfaceView;
import com.example.tundeadegoroye.adaptrush.R;

public class GameActivity extends AppCompatActivity {

   // Variable for background music
   protected MediaPlayer bgMusic;

    // Variable for layout
   protected RelativeLayout relLayout;

   // Variable for images on the game view
   protected ImageView environmentImgVw, healthImg1, healthImg2, healthImg3;

   // Variable for holding the game
   protected GameSurfaceView gameSurfaceView;

   // Variable for holding sensor values
   protected SensorManager sensorManager;

   // Variable for holding the score
   protected TextView scoreTxtVw, feedbackTxtVw;

   // Anon class listener for sensor
   protected SensorEventListener sensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {

            // Set the value based on tilt of phone
            gameSurfaceView.setTilt(event.values[0]);
        }
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Create the background music for the game
        bgMusic = MediaPlayer.create(this, R.raw.gamemusic);
        bgMusic.setLooping(true);
        bgMusic.start();

        // Find UI components
        environmentImgVw = (ImageView) findViewById(R.id.enviromentImgVw);
        scoreTxtVw = (TextView) findViewById(R.id.scoreTxtVw);
        feedbackTxtVw = (TextView) findViewById(R.id.answerTxtVw);

        healthImg1 = (ImageView) findViewById(R.id.healthImgVw1);
        healthImg2 = (ImageView) findViewById(R.id.healthImgVw2);
        healthImg3 = (ImageView) findViewById(R.id.healthImgVw3);

        relLayout = (RelativeLayout) findViewById(R.id.gameView);

        // Instantiate the game with UI components and the current activity
        gameSurfaceView = new GameSurfaceView(this, environmentImgVw, this, scoreTxtVw, new ImageView[] {healthImg1, healthImg2, healthImg3}, feedbackTxtVw);

        // Add the game to the layout in this view
        relLayout.addView(gameSurfaceView);

        // Get the sensor and register the listener with: listener, accelerometer & time to check for sensor
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(sensorListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Depending the item pressed either: pause both the music & game or resume both the music & game or stop the game and go to the home screen
        switch (item.getItemId()){
            case R.id.pause:
                bgMusic.pause();
                gameSurfaceView.stop();
                break;
            case R.id.resume:
                bgMusic.start();
                gameSurfaceView.start();
                break;
            case R.id.quit:
                gameSurfaceView.stop();
                Intent i = new Intent(this, StartActivity.class);
                startActivity(i);
                break;
        }

        // Dispose and start the activity
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        bgMusic.start();
        gameSurfaceView.start();
    }

    // Stop everything when user closes the app
    @Override
    protected void onStop(){
        super.onStop();

        bgMusic.pause();
        gameSurfaceView.stop();

    }

    // Stop everything when the user dismiss the app
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();

        bgMusic.pause();
        gameSurfaceView.stop();
    }

    @Override
    public void onBackPressed() {
    }
}
