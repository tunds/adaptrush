package com.example.tundeadegoroye.adaptrush.Models;

/**
 * Created by tundeadegoroye on 21/11/2016.
 */

public class User {

    // Variable to hold the name
    private String name;

    // Variable for holding the score
    private int score;

    public User(String name, int score) {
        this.name = name;
        this.score = score;
    }

    /**
     * This method is used to get the user name
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * This method is used to set the user's name
     * @param name The name of the user
     * @return null
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method is used to get the score from the user
     * @return int
     */
    public int getScore() {
        return score;
    }

}
