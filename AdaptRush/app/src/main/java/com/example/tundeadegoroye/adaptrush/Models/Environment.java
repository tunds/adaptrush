package com.example.tundeadegoroye.adaptrush.Models;

import android.graphics.drawable.Drawable;

/**
 * Created by tundeadegoroye on 27/11/2016.
 */

public class Environment {

    // The image for the environment
    private Drawable image;

    // The array of animals for the environment
    private String[] animals;

    public Environment(Drawable image, String[] animals) {
        this.image = image;
        this.animals = animals;
    }

    /**
     * This method is used to get the environment image
     * @return Drawable
     */
    public Drawable getImage() {
        return image;
    }

    /**
     * This method is used to get the animals for that environment
     * @return String[]
     */
    public String[] getAnimals() {
        return animals;
    }
}
