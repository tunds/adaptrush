package com.example.tundeadegoroye.adaptrush.Controllers;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.tundeadegoroye.adaptrush.Controllers.InstructionsActivity;
import com.example.tundeadegoroye.adaptrush.Controllers.LeaderboardsActivity;
import com.example.tundeadegoroye.adaptrush.Controllers.StartActivity;
import com.example.tundeadegoroye.adaptrush.Controllers.AboutActivity;
import com.example.tundeadegoroye.adaptrush.Controllers.GameActivity;
import com.example.tundeadegoroye.adaptrush.R;

/**
 * Created by tundeadegoroye on 17/11/2016.
 */

public class Misc extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    // Variable to move between activities
    private Intent i;

    // Variable for background music
    private MediaPlayer bgMusic;

    // When the app reopens
    @Override
    protected void onResume() {
        super.onStart();

        // Play if the music isn't already playing and release any other objects
        if (!bgMusic.isPlaying()) {
            bgMusic.release();
            bgMusic.start();
        }
    }

    // When the app is going back to another activity
    @Override
    protected void onPause() {
        super.onPause();

        bgMusic.stop();
    }

    // When the activity starts create the mediaplayer, loop it and start it
    @Override
    protected void onStart() {
        super.onStart();

        bgMusic = MediaPlayer.create(this, R.raw.bgmusic);
        bgMusic.setLooping(true);
        bgMusic.start();

    }

    /**
     * This method is used to go to the game
     * @return null
     */
    public void startGame(){

        // Set the intent, dismiss this activity and go to the game
        i = new Intent(this, GameActivity.class);
        finish();
        startActivity(i);
    }

    /**
     * This method is used to go to the instruction screen
     * @return null
     */
    public void startInstructions(){

        // Set the intent, dismiss this activity and go to the game
        i = new Intent(this, InstructionsActivity.class);
        finish();
        startActivity(i);
    }

    // Gen code to manage navigation drawer
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        // Based on the button pressed go the activity
        switch (item.getItemId()){
            case R.id.nav_play:
                startGame();
                break;
            case R.id.nav_learn:
                i = new Intent(this, InstructionsActivity.class);
                break;
            case R.id.nav_leaderboards:
                i = new Intent(this, LeaderboardsActivity.class);
                break;
            case R.id.nav_start:
                i = new Intent(this, StartActivity.class);
                break;
            case R.id.nav_about:
                i = new Intent(this, AboutActivity.class);
                break;
            case R.id.nav_settings:
                i = new Intent(this, SettingsActivity.class);
                break;
        }

        // Close the drawer, dismiss this activity and go to the selected activity
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        finish();
        startActivity(i);

        return true;
    }
}
