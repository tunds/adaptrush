package com.example.tundeadegoroye.adaptrush.Models;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by tundeadegoroye on 23/11/2016.
 */

public class AudioManager extends AsyncTask<MediaPlayer, Void, Void> {

    @Override
    protected Void doInBackground(MediaPlayer... params) {

        // If the sound isn't already playing play it
        if (!params[0].isPlaying()) {
             params[0].start();
        }
        return null;
    }


}
