package com.example.tundeadegoroye.adaptrush.Models;

import android.os.AsyncTask;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import java.util.Random;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by tundeadegoroye on 01/12/2016.
 */

public class FeedbackManager extends AsyncTask<Boolean, Void, Boolean> {

    // Textview for the feedback for the user
    private TextView textView;

    public FeedbackManager(TextView textView) {
        this.textView = textView;
    }

    // Return the status true or false
    @Override
    protected Boolean doInBackground(Boolean... params) {
        return params[0];
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        // Display the feedback based on the answer
        if (aBoolean){
            animateFeedback(textView, correctFeedback());
        } else {
            animateFeedback(textView, wrongFeedback());
        }
    }

    /**
     * This method is used to animate the feedback after an answer
     * @param textView The text view to put the feedback in game object
     * @param feedback The feedback you want to give
     * @return null
     */
    private void animateFeedback(final TextView textView, String feedback){

        // Show the textview and set the text
        textView.setVisibility(VISIBLE);
        textView.setText(feedback);

        // Animations to move the position
        final TranslateAnimation slideForwardAni = new TranslateAnimation(-500f, 200f, 0f, 0f);
        final TranslateAnimation slideBackAni = new TranslateAnimation(200f, -500f, 0f, 0f);

        // Set the time for each animations
        slideForwardAni.setDuration(1500);
        slideBackAni.setDuration(1500);

        // Start the animation to move the textview to the left
        textView.startAnimation(slideForwardAni);

        // Set a listener to check when it's finished move it back
            textView.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {

                    textView.startAnimation(slideBackAni);

                    // Set another listener to check when finished hide the textview
                    textView.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {}

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            textView.setVisibility(INVISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {}
                    });
                }

                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
    }

    /**
     * This method is used to get a correct feedback to display
     * @return String
     */
    private String correctFeedback(){

        String[] feedback = new String[] {"On a roll", "You smart", "Awesome", "Good job", "Killin it", "Ayeee"};
        return feedback[new Random().nextInt(feedback.length)];
    }

    /**
     * This method is used to get a wrong feedback to display
     * @return String
     */
    private String wrongFeedback(){

        String[] feedback = new String[] {"Ooops", "Maybe next time", "Almost", "Good try", "Don't worry it"};
        return feedback[new Random().nextInt(feedback.length)];
    }

}
