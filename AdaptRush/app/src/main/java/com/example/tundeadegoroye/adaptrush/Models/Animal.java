package com.example.tundeadegoroye.adaptrush.Models;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

/**
 * Created by tundeadegoroye on 27/11/2016.
 */

public class Animal {

    // Variables for position on canvas ( x & y ) and direction ( dx & dy )
    private float x, y, dx = 1, dy = 1;

    // Variable for image
    private Drawable image;

    // Variable for holding information about the animal
    private String animalType, info;

    public Animal(float dx, float dy, float x, float y, Drawable image, String animalType, String info) {
        this.dx = dx;
        this.dy = dy;
        this.image = image;
        this.x = x;
        this.y = y;
        this.animalType = animalType;
        this.info = info;
    }

    /**
     * This method is used to move the object on the canvas
     * @param canvas The canvas to draw onto
     * @param tilt  The direction to move the object on the canvas
     * @return null
     */
    public void moveGameObject(Canvas canvas, float tilt){

        // Depending on the rotation of the phone either move left, straight or right on the canvas
        if(tilt > 3) {
            x = x - dx;
        } else if(tilt < -3) {
            x = x + dx;
        } else {
            x = x + 0;
        }

        // Move object downwards
        y = y + dy;

        // If the game object hits the sides bounce of the sides
        if(x>canvas.getWidth() - 150 || x < 0)
            dx =- dx;
        if(y>canvas.getHeight() - 150 || y < -200)
            dy =- 0;

        // Set the position of the image and draw onto the canvas
        image.setBounds((int)x,(int)y,(int)(x+200f),(int)(y+200f));
        image.draw(canvas);

    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    /**
     * This method is used to get information about the animal object
     * @return String
     */
    public String getInfo() {
        return info;
    }

    /**
     * This method is used to get the type of animal object
     * @return String
     */
    public String getAnimalType() {
        return animalType;
    }

    /**
     * This method is used to get the image of the animal object
     * @return Drawable
     */
    public Drawable getImage() {
        return image;
    }

    /**
     * This method is used to set the position of the animal object on the x axis
     * @param x The position of the object on the x axis
     * @return null
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * This method is used to set the position of the animal object on the y axis
     * @param y The position of the object on the y axis
     * @return null
     */
    public void setY(float y) {
        this.y = y;
    }


}
