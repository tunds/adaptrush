package com.example.tundeadegoroye.adaptrush;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tundeadegoroye.adaptrush.Controllers.GameOverActivity;
import com.example.tundeadegoroye.adaptrush.Models.Animal;
import com.example.tundeadegoroye.adaptrush.Models.FeedbackManager;
import com.example.tundeadegoroye.adaptrush.Models.VerifyAnswer;
import com.example.tundeadegoroye.adaptrush.Models.AudioManager;
import com.example.tundeadegoroye.adaptrush.Models.Environment;
import com.example.tundeadegoroye.adaptrush.Models.GameObject;
import com.example.tundeadegoroye.adaptrush.Models.Health;
import com.example.tundeadegoroye.adaptrush.Models.Score;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Random;


/**
 * Created by tundeadegoroye on 18/11/2016.
 */

public class GameSurfaceView extends SurfaceView implements Runnable {

    // Music sounds for wrong and correct
    private MediaPlayer correct, wrong;

    // Game object variables
    private float tilt = 0, genNum;

    private SurfaceHolder mySurfaceHolder;
    private Thread myThread;

    private boolean isRunning = true, isCorrect, isRespawning = false;

    private GameObject gameObjects;
    private Environment environment;

    private Animal animal;

    private VerifyAnswer verifyAnswer;

    private Score score;
    private Health health;

    private Context context;
    private Paint pWhite = new Paint();

    private ImageView environmentImg;
    private ImageView[] healthImgs;

    private TextView answerTxtVw;

    // Intent for game over
    private Intent gameOverIntent;

    // Thread for game sounds
    private Activity gameActivity;

    public GameSurfaceView(Context context, ImageView imageView, Activity activity, TextView scoreTxtVw, ImageView[] images, TextView answerTxtVw) {
        super(context);

        // Get and create the audio for game sounds
        correct = MediaPlayer.create(context, R.raw.correct);
        wrong = MediaPlayer.create(context,R.raw.incorrect);

        // Instantiate objects for the game
        gameObjects = new GameObject(context);
        score = new Score(scoreTxtVw);
        health = new Health(images, context);

        // Instantiate Game over intent
        gameOverIntent = new Intent(context, GameOverActivity.class);

        // Start the game with an  environment and animal
        environment = getEnvironment();
        animal = getAnimal();

        // Configure objects in this class
        this.healthImgs = images;
        this.context = context;
        gameActivity = activity;
        environmentImg = imageView;

        // Set the environment background
        environmentImg.setImageDrawable(environment.getImage());

        // Get the surface the canvas will be drawn on
        mySurfaceHolder = getHolder();

        // Start the thread
        myThread = new Thread(this);
        myThread.start();

        // Set the color of the background
        pWhite.setColor(Color.WHITE);

        // Set the answer textview
        this.answerTxtVw = answerTxtVw;
        this.answerTxtVw.setVisibility(INVISIBLE);

    }

    @Override
    public void run() {


        // The game's update function, will run every frame as long as this is true
        while (isRunning) {



            // Go back to the if statement and check if the surface still isn't valid
            if (!mySurfaceHolder.getSurface().isValid())
                continue;

            // Lock the canvas for editing, make changes and unlock with new changes
            final Canvas canvas = mySurfaceHolder.lockCanvas();

            // Used later to create different spawn points
            genNum = canvas.getWidth();

            // Draw the game canvas
            canvas.drawRect(0,0,canvas.getWidth(), canvas.getHeight(), pWhite);

            // Instantiate verification and move game object
            verifyAnswer = new VerifyAnswer(canvas);
            animal.moveGameObject(canvas, tilt);

            // Make sure that the animal is close to the answer block
            if(verifyAnswer.onCollision(animal) != null){

                isRespawning = true;

                // Inside of box else is outside of box
                if (verifyAnswer.onCollision(animal)) {

                    // If the animal is part of that environment
                    if (verifyEnvironment(environment.getAnimals())){
                        isCorrect = true;
                    } else {
                        isCorrect = false;
                    }

                } else {

                    // If the animal is part of that environment
                    if (!verifyEnvironment(environment.getAnimals())){
                        isCorrect = true;
                    } else {
                        isCorrect = false;
                    }
                }

                animal.setY(-200);
                if (isRespawning) {

                    Log.v("Spawn:","Tessssss");

                    isRespawning = false;

                    // The UI thread
                    gameActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // Depending on the answer play the sound and change the score
                            if (isCorrect) {

                                new FeedbackManager(answerTxtVw).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, true);
                                new AudioManager().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, correct);
                                score.incrementScore(10);

                            } else if (!isCorrect) {

                                new FeedbackManager(answerTxtVw).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, false);
                                new AudioManager().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, wrong);
                                health.decreaseHealth(10);
                                health.decreaseHealthImgs(health.getHealth());

                                // Stop the game
                                if (health.getHealth() <= 0) {
                                    isRunning = false;
                                }

                            }

                            resetGameObjects();

                        }
                    });
                }
            }

            // Post changes to the canvas
            mySurfaceHolder.unlockCanvasAndPost(canvas);
        }

        // If the user is dead put the score in the intent and go to the game over activity
        if (health.getHealth() <= 0){
            gameOverIntent.putExtra("Score", score.getScore());
            context.startActivity(gameOverIntent);
        }
    }

    /**
     * This method is used to set the title variable based on accelerometer
     * @param tilt The tilt of the phone
     * @return null
     */
    public void setTilt(float tilt) {
        this.tilt = tilt;
    }

    /**
     * This method is used to assign a new animal and environment object
     * @return null
     */
   private void resetGameObjects(){

        // Get the environment and set the image
        environment = getEnvironment();
        environmentImg.setImageDrawable(environment.getImage());

        // Get a new animal, set a new position on the x axis based on canvas width, set y to 0 (top of canvas)
        animal = getAnimal();
        animal.setX(new Random().nextInt((int) (genNum/2)));
        animal.setY(-200);

    }

    /**
     * This method is used to get a new environment
     * @return Environment
     */
    private Environment getEnvironment(){

        // Convert the hashmap into an array list
        // Get an object from the array at a random index
        ArrayList<Environment> environmentObject = new ArrayList<>(gameObjects.getGameObjectMap().keySet());
        return environmentObject.get(new Random().nextInt(environmentObject.size()));
    }

    /**
     * This method is used to get a new animal
     * @return Animal
     */
   private Animal getAnimal(){

        ArrayList<Animal> animalObjects = new ArrayList<>(gameObjects.getGameObjectMap().values());
       return animalObjects.get(new Random().nextInt(animalObjects.size()));
    }

    /**
     * This method is used to verify if the animals live in the environment
     * @param animals The animal game object
     * @return boolean
     */
    private boolean verifyEnvironment(String[] animals){

        return Arrays.asList(animals).contains(animal.getAnimalType());
    }

    /**
     * This method is used to stop the game
     * @return null
     */
    public void stop()
    {
        // Variable to stop the loop
        isRunning = false;

        while(true)
        {
            try {
                myThread.join();
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            break;
        }
    }

    /**
     * This method is used to start the game
     * @return null
     */
    public void start()
    {
        isRunning = true;
        myThread = new Thread(this);
        myThread.start();
    }


}
