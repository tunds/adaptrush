package com.example.tundeadegoroye.adaptrush.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tundeadegoroye.adaptrush.R;
import com.example.tundeadegoroye.adaptrush.Models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tundeadegoroye on 22/11/2016.
 * This is the adapter to fill a list with scores
 */

public class ScoreAdapter extends ArrayAdapter<User> {

    // ArrayList to hold scores
    private ArrayList<User> userScores;

    // Layout variable
    private LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    // High score image and activity variable
    private Drawable highScoreImg;
    private Context context;

    public ScoreAdapter(Context context, int resource, List<User> objects) {
        super(context, resource, objects);

        this.context = context;
        this.userScores = (ArrayList<User>) objects;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // If the layout is empty get the list xml file for scores
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_score_item_view, null);

        // Depending on the position of the item phoneAnimatorSet the high score image
        switch (position){
            case 0:
                highScoreImg = ContextCompat.getDrawable(context, R.drawable.glove);
                break;
            case 1:
                highScoreImg = ContextCompat.getDrawable(context, R.drawable.silver);
                break;
            case 2:
                highScoreImg = ContextCompat.getDrawable(context, R.drawable.bronze);
                break;
            default:
                highScoreImg = null;
                break;
        }


        // Get a user from the ArrayList
        User user = userScores.get(position);

        // Assign the UI components with their values
        ((ImageView)convertView.findViewById(R.id.scoreImg)).setImageDrawable(highScoreImg);
        ((TextView)convertView.findViewById(R.id.scoreItemTextVw)).setText(position + 1 + ": " + user.getName() + "   Score: " + user.getScore());

        // Send back the layout view
        return convertView;
    }
}
