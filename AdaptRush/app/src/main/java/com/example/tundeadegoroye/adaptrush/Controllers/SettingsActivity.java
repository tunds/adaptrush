package com.example.tundeadegoroye.adaptrush.Controllers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.tundeadegoroye.adaptrush.Models.DatabaseManager;
import com.example.tundeadegoroye.adaptrush.R;

public class SettingsActivity extends Misc {

    // Buttons for the difficulty
    Button easyBtn;
    Button medBtn;
    Button hardBtn;

    // Buttons to reset the score
    Button resetBtn;

    // The database variable
    protected DatabaseManager db;

    // Difficulty listener
    View.OnClickListener difficultyListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()){

                // Change the difficulty in the database
                case R.id.easyBtn:
                    db.changeDifficulty("Easy");
                    tintButton(easyBtn, new Button[] {medBtn, hardBtn});

                    break;
                case R.id.medBtn:
                    db.changeDifficulty("Medium");
                    tintButton(medBtn, new Button[] {easyBtn, hardBtn});
                    break;
                case R.id.hardBtn:
                    db.changeDifficulty("Hard");
                    tintButton(hardBtn, new Button[] {medBtn, easyBtn});
                    break;
            }
        }
    };

    // Reset listener
    View.OnClickListener resetListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            db.deleteScores();
            Toast.makeText(getApplicationContext(), "Scores have been reset", Toast.LENGTH_SHORT).show();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_navigation_draw);

        // Get the database
        db = new DatabaseManager(this);

        // Get the buttons for the difficulty
        easyBtn = (Button) findViewById(R.id.easyBtn);
        medBtn = (Button) findViewById(R.id.medBtn);
        hardBtn = (Button) findViewById(R.id.hardBtn);

        // Get the button for the reset
        resetBtn = (Button) findViewById(R.id.resetBtn);

        // Set the listeners
        resetBtn.setOnClickListener(resetListener);
        easyBtn.setOnClickListener(difficultyListener);
        medBtn.setOnClickListener(difficultyListener);
        hardBtn.setOnClickListener(difficultyListener);

        // Change buttons tint based on difficulty
        switch (db.getDifficulty()){
            case "Easy":
                tintButton(easyBtn, new Button[] {medBtn, hardBtn});
                break;
            case "Medium":
                tintButton(medBtn, new Button[] {easyBtn, hardBtn});
                break;
            case "Hard":
                tintButton(hardBtn, new Button[] {medBtn, easyBtn});
                break;
        }

        // Gen code for the navigation draw
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void tintButton(Button activeButton, Button[] disabledButtons){

        activeButton.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorDarkAccent));

        for (Button button : disabledButtons){
            button.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
        }
    }

}
