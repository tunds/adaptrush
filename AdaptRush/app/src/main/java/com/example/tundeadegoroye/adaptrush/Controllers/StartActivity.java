package com.example.tundeadegoroye.adaptrush.Controllers;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.example.tundeadegoroye.adaptrush.Models.DatabaseManager;
import com.example.tundeadegoroye.adaptrush.R;

public class StartActivity extends Misc {

   // Image view for the phone
   protected ImageView phoneImgVw;

   // The database variable
   protected DatabaseManager db;

   // Animator for phone image
   protected AnimatorSet phoneAnimatorSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_navigation_draw);

        // Get the database
        db = new DatabaseManager(this);

        // Get UI components & call function to start animation
        phoneImgVw = (ImageView) findViewById(R.id.smartPhoneImg);
        rotatePhone();

        // When the user presses the play button start the game
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Check if it's the users first time
                if (db.verifyFirstTime()){
                    startInstructions();
                } else {
                    startGame();
                }
            }
        });

        // Gen code for the navigation draw
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    /**
     * This method is used to create a animation of phone rotating left to right which loops forever
     * @return null
     */
    private void rotatePhone(){

        // Create animations
        ObjectAnimator rotateLeft = ObjectAnimator.ofFloat(phoneImgVw, "rotation", -45).setDuration(1200);
        ObjectAnimator rotateRight = ObjectAnimator.ofFloat(phoneImgVw, "rotation", 45).setDuration(1200);

        // Create an animator set, play the left then right and start it
        phoneAnimatorSet = new AnimatorSet();
        phoneAnimatorSet.play(rotateLeft).after(rotateRight);
        phoneAnimatorSet.start();

        // Add the listener to the animator set and when the animation ends restart it
        phoneAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {
                phoneAnimatorSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
    }
}
