package com.example.tundeadegoroye.adaptrush.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tundeadegoroye.adaptrush.R;


public class EnvironmentFragment extends Fragment {

    // Text fields for assigning data for the text field
    private TextView titleTxtVw,infoTxtVw;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Get the UI components
        infoTxtVw = (TextView) getActivity().findViewById(R.id.enviromentInfo);
        titleTxtVw = (TextView) getActivity().findViewById(R.id.enviromentTitle);

        // if the arguments aren't empty
        if (getArguments() != null) {

            // Set the the text labels
            titleTxtVw.setText(getArguments().getString("title"));
            infoTxtVw.setText(getArguments().getString("info"));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_enviroment, container, false);
    }

}
