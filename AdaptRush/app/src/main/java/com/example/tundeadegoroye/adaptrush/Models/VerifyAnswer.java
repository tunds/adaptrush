package com.example.tundeadegoroye.adaptrush.Models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by tundeadegoroye on 18/11/2016.
 */

public class VerifyAnswer {

    // The answer block & color variable
    private Rect rectangle;
    private Paint paint;

    public VerifyAnswer(Canvas canvas) {

        // Create a yellow rectangle somewhere around the middle
        rectangle = new Rect( (int)(canvas.getWidth() * 0.3), canvas.getHeight(), (int)(canvas.getWidth() * 0.7), canvas.getHeight() - 100);
        paint = new Paint();
        paint.setColor(Color.YELLOW);
        canvas.drawRect(rectangle, paint);
    }

    /**
     * This method is used to check if the game object has hit the rectangle
     * @param animal The animal game object
     * @return Boolean
     */
    public Boolean onCollision(Animal animal){

        // This is null if the object isn't anywhere near the answer block
        Boolean insideOfBounds = null;

        // If the object is within the top and bottom
        if(animal.getImage().getBounds().top < rectangle.top && animal.getImage().getBounds().bottom > rectangle.bottom){

            // If the object is within the left and right
            if(animal.getImage().getBounds().left + 100 > rectangle.left && animal.getImage().getBounds().right < rectangle.right + 100){
                insideOfBounds = true;
            } else {
                insideOfBounds = false;
            }
        }

        return insideOfBounds;
    }

}