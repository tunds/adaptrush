package com.example.tundeadegoroye.adaptrush.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tundeadegoroye on 21/11/2016.
 *
 */

public class DatabaseManager extends SQLiteOpenHelper {

    // Help from http://www.androidhive.info/2011/11/android-sqlite-database-tutorial/
    // Configs for the database
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "scoreManager";
    private static final String TABLE_LEADERBOARDS = "leaderboard";
    private static final String TABLE_CONFIG = "config";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_SCORE = "score";
    private static final String KEY_FIRST_TIME = "first_time";
    private static final String KEY_DIFFICULTY = "difficulty";

    public DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create the database with the sql command
        String CREATE_CONFIG_TABLE = "CREATE TABLE " + TABLE_CONFIG + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_FIRST_TIME + " BOOLEAN, " + KEY_DIFFICULTY + " TEXT " + ")";

        String CREATE_LEADERBOARDS_TABLE = "CREATE TABLE " + TABLE_LEADERBOARDS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + KEY_NAME + " TEXT,"
                + KEY_SCORE + " INTEGER" + ")";

        db.execSQL(CREATE_LEADERBOARDS_TABLE);
        db.execSQL(CREATE_CONFIG_TABLE);

        addDefaultConfig(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    /**
     * This method is used to insert a the default config to check if it's the users first time
     * @return null
     */
    public void addDefaultConfig(SQLiteDatabase db) {


        if (db.isOpen()) {

            // Get the database to write to it and the values
            ContentValues firstTimeValue = new ContentValues();
            // Insert into the columns the default values for the game
            firstTimeValue.put(KEY_FIRST_TIME, true);

            // Get the database to write to it and the values
            ContentValues difficultyValue = new ContentValues();
            difficultyValue.put(KEY_DIFFICULTY, "Easy");
            // Insert the database and close the connection
            db.insert(TABLE_CONFIG, null, firstTimeValue);
            db.insert(TABLE_CONFIG, null, difficultyValue);
          //  db.close();
        }

    }

    /**
     * This method is used to insert a new score into the database
     * @param user The user object to insert
     * @return null
     */
    public void addScore(User user) {

        // Get the database to write to it and the values
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        // Insert into the columns the name and score
        contentValues.put(KEY_NAME, user.getName());
        contentValues.put(KEY_SCORE, user.getScore());

        // Insert the database and close the connection
        db.insert(TABLE_LEADERBOARDS, null, contentValues);
        db.close();

    }

    /**
     * This method is used get the top 10 scores from the database
     * @return List<User>
     */
    public List<User> getScores(){

        // List for holding the scores
        List<User> scoreList = new ArrayList<>();

        // The query to run
        String selectQuery = "SELECT  * FROM " + TABLE_LEADERBOARDS + " ORDER BY " + KEY_SCORE + " DESC LIMIT 10";

        // Get the database to write to and query it
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Get the first row
        if (cursor.moveToFirst()) {

            // While there is a row in the variable
            do {

                // Add a new user to the list
                scoreList.add(new User(cursor.getString(1), Integer.parseInt(cursor.getString(2))));

            } while (cursor.moveToNext());
        }

        return scoreList;
    }

    public void deleteScores(){

        // Get the database to write to and query it
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LEADERBOARDS, null, null);
        db.close();

    }


    public boolean verifyFirstTime(){

        // The query to run
        String selectQuery = "SELECT * FROM " + TABLE_CONFIG + " WHERE " + KEY_ID + " = 1";

        // Get the database to write to and query it
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Get the first row
        if (cursor.moveToFirst()) {

            // While there is a row in the variable
            do {

                if (cursor.getInt(cursor.getColumnIndex(KEY_FIRST_TIME)) > 0)
                    return true;
            } while (cursor.moveToNext());
        }

        return false;
    }

    public String getDifficulty(){

        // The query to run
        String selectQuery = "SELECT " + KEY_DIFFICULTY + " FROM " + TABLE_CONFIG;
        String difficulty = "";

        // Get the database to write to and query it
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // Get the first row
        if (cursor.moveToFirst()) {

            // While there is a row in the variable
            do {

                difficulty = cursor.getString(0);

            } while (cursor.moveToNext());
        }

        return difficulty;
    }

    public void changeFirstTime(){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_TIME, false);
        // updating row
        db.update(TABLE_CONFIG, values, KEY_ID + " = ?", new String[] { "1" });

    }

    public void changeDifficulty(String difficulty){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DIFFICULTY, difficulty);
        // updating row
        db.update(TABLE_CONFIG, values, KEY_ID + " != ?", new String[] { "" });

    }
}
