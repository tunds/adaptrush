package com.example.tundeadegoroye.adaptrush.Controllers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.example.tundeadegoroye.adaptrush.Fragments.AnimalFragment;
import com.example.tundeadegoroye.adaptrush.Fragments.EnvironmentFragment;
import com.example.tundeadegoroye.adaptrush.Models.Animal;
import com.example.tundeadegoroye.adaptrush.Models.DatabaseManager;
import com.example.tundeadegoroye.adaptrush.R;
import java.util.ArrayList;

public class InstructionsActivity extends Misc {

    // Bundle to pass data
    protected Bundle bundle;

    // Database class
    protected DatabaseManager db;

    // Fragments
    protected EnvironmentFragment environmentFragment;
    protected AnimalFragment animalFragment;
    protected FragmentManager fragmentManager;

    // Image buttons for managing fragments
    protected ImageButton coldImgBtn,desertImgBtn,sunnyImgBtn;

    // Anon class listener for environment
    protected View.OnClickListener environmentListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // Array for holding animal arrays
            ArrayList<Animal> animals = new ArrayList<>();

            // Reset when button is pressed
            animals.clear();

            // Based on the button phoneAnimatorSet the animals array and the enviroment
            switch (v.getId()){

                case R.id.coldImgBtn:
                    animals.add(new Animal(0,0,0,0,ContextCompat.getDrawable(getApplicationContext(), R.drawable.penguin),"penguin", "Penguins are a group of aquatic, flightless birds living almost exclusively in the Southern Hemisphere, especially in Antarctica."));
                    setEnvironmentInfo("Arctic", "Polar habitats are located at the very top and very bottom of the Earth. They are cold, windy and have a lot of snow and ice. It’s even too cold for trees to grow.", animals);
                    break;
                case R.id.desertImgBtn:
                    animals.add( new Animal(0,0,0,0,ContextCompat.getDrawable(getApplicationContext(), R.drawable.panther),"panther", "A black panther is the melanistic color variant of any Panthera species. Black panthers in Asia and Africa are leopards (Panthera pardus) and black panthers in the Americas are black jaguars (Panthera onca)."));
                    animals.add(new Animal(0,0,0,0,ContextCompat.getDrawable(getApplicationContext(), R.drawable.rhino),"rhino","A rhinoceros, often abbreviated to rhino, is one of any five extant species of odd-toed ungulates in the family Rhinocerotidae, as well as any of the numerous extinct species."));
                    animals.add(new Animal(0,0,0,0,ContextCompat.getDrawable(getApplicationContext(), R.drawable.camel),"camel","A camel is an even-toed ungulate within the genus Camelus, bearing distinctive fatty deposits known as 'humps' on its back."));
                    setEnvironmentInfo("Desert", "Deserts are places that don’t get much rain, and are very dry. They can be either hot places, or cold places.", animals);
                    break;
                case R.id.sunnyImgBtn:
                    animals.add(new Animal(0,0,0,0,ContextCompat.getDrawable(getApplicationContext(), R.drawable.rabbit),"rabbit", "Rabbits are small mammals in the family Leporidae of the order Lagomorpha, found in several parts of the world."));
                    setEnvironmentInfo("Grasslands", "Grasslands are areas dominated by grasses and forbs, and have few or no trees. Grazing and roaming animals occur in abundance.", animals);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.learn_navigation_draw);

        // Get the database
        db = new DatabaseManager(this);

        // Get the UI components
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        coldImgBtn = (ImageButton) findViewById(R.id.coldImgBtn);
        desertImgBtn = (ImageButton) findViewById(R.id.desertImgBtn);
        sunnyImgBtn = (ImageButton) findViewById(R.id.sunnyImgBtn);

        // Set the button click listeners
        coldImgBtn.setOnClickListener(environmentListener);
        desertImgBtn.setOnClickListener(environmentListener);
        sunnyImgBtn.setOnClickListener(environmentListener);

        // Gen code for the navigation draw
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Display or hide button based on if it's a new user
        if (db.verifyFirstTime()) {

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   db.changeFirstTime();
                   startGame();
                }
            });

        } else {

            fab.hide();

        }


    }

    /**
     * This method is used to phoneAnimatorSet up the environment image and the
     * correct animals using fragments
     * @param title This is the first paramter to phoneAnimatorSet the environment title
     * @param info  This is the second parameter to phoneAnimatorSet the environmenta information
     * @param animals  This is the third parameter to assign the animals
     * @return null
     */
    private void setEnvironmentInfo(String title, String info, ArrayList<Animal> animals){

        // Create a new bundle and fragment
        bundle = new Bundle();
        environmentFragment = new EnvironmentFragment();
        animalFragment = new AnimalFragment();

        // Set the title and info to pass into the fragment
        bundle.putString("title", title);
        bundle.putString("info", info);
        environmentFragment.setArguments(bundle);

        // Pass the arraylist to the fragment for the recycle view
        animalFragment.setAnimals(animals);

        // Get the fragment and and replace the content with both the environment and animal fragment
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.enviromentInfoContainer, environmentFragment)
                .replace(R.id.animalsInfoContainer, animalFragment)
                .commit();
    }
}
