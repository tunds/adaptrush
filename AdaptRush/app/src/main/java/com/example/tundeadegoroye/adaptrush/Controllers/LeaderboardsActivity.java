package com.example.tundeadegoroye.adaptrush.Controllers;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.example.tundeadegoroye.adaptrush.Adapters.ScoreAdapter;
import com.example.tundeadegoroye.adaptrush.Models.DatabaseManager;
import com.example.tundeadegoroye.adaptrush.R;

public class LeaderboardsActivity extends Misc {

   // Adapter and list for displaying the scores
   protected ScoreAdapter scoreAdapter;
   protected ListView scoreListVw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaderboards_navigation_draw);

        // Get the UI components
        scoreListVw = (ListView) findViewById(R.id.scoreList);
        scoreListVw.setEmptyView(findViewById(R.id.empty));

        // Instantiate the adapter for the score and assign it to the listview
        scoreAdapter = new ScoreAdapter(this, android.R.layout.simple_list_item_1, new DatabaseManager(this).getScores());
        scoreListVw.setAdapter(scoreAdapter);

        // Gen code for the navigation draw
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }
}
