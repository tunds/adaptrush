package com.example.tundeadegoroye.adaptrush.Models;

import android.widget.TextView;

/**
 * Created by tundeadegoroye on 19/11/2016.
 */

public class Score {

    // The score variable
    private int score = 0;

    // The textview to display the score
    private TextView scoreTxtVw;

    public Score(TextView scoreTxtVw) {
        this.scoreTxtVw = scoreTxtVw;
    }

    /**
     * This method is used to get the score
     * @return int
     */
    public int getScore() {
        return score;
    }

    /**
     * This method is used to increment the score
     * @param score The amount to add onto the score
     * @return null
     */
    public void incrementScore(int score) {
        this.score += score;
        scoreTxtVw.setText("Score: " + getScore());
    }
}
