package com.example.tundeadegoroye.adaptrush.Models;

/**
 * Created by tundeadegoroye on 08/12/2016.
 */

public class Config {

    private String difficulty = "Easy";


    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }
}
